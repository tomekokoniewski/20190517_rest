package pl.com.tok._restrest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.json.JSONArray;
import org.json.JSONObject;

//http://localhost:8090/api/wolves/blue
//http://localhost:8090/api/wolves
//http://localhost:8090/api/wolves/0

@Path("wolves")
public class WolfResource {

    private static final Wolf[] wolves
            = {new Wolf("Blue", 7), new Wolf("Red", 4)};

    private JSONObject parse(Wolf wolf) {
        JSONObject json = new JSONObject();
        json.put("name", wolf.getName());
        json.put("age", wolf.getAge());
        return json;
    }

    @Path("blue")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getBlue() {
        return parse(wolves[0]).toString();
    }

    @Path("")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getWolves() {
        JSONArray array = new JSONArray();
        for (Wolf wolf : wolves) {
            array.put(parse(wolf));
        }
        return array.toString();
    }



        @Path("{id:[0-9]+}") //pattern aby podawane były tylko liczby
        @GET
        @Produces(MediaType.APPLICATION_JSON)
        public String getWolf(@PathParam("id") int id) {
            return parse(wolves[id]).toString();
        }
}

