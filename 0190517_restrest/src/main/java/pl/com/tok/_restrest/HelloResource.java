package pl.com.tok._restrest;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

/**
 * http://localhost:8090/api/hello/user/12
 * http://localhost:8090/api/hello/user/asfd?count=10
 */
@Path("hello")
public class HelloResource {

    @Path("user/{login:[a-z]+}")
    @GET
    public String hello(@PathParam("login")String login,
            @QueryParam("count") @DefaultValue("1") int count) {
        String ret ="";
        for (int i = 0; i < count; i++) {
            ret += "hello "+login+"\n";
        }
        return ret;
    }

    @Path("user/{id:[0-9]+}")
    @GET
    public String hello(@PathParam("id")int id) {
        return "HI there my userID! "+id;
    }
    

    
}
