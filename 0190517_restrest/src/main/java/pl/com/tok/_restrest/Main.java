package pl.com.tok._restrest;

import java.net.URI;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

public class Main {

    public static void main(String[] args) {
        JdkHttpServerFactory.createHttpServer(
                URI.create("http://localhost:8090/api"),
                new ResourceConfig(HelloResource.class, Test.class, WolfResource.class, ExpenseResouce.class, CorsFilter.class));
    }

}
