package pl.com.tok._restrest;

//http://localhost:8090/api/categories
//http://localhost:8090/api/categories/1
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.json.JSONArray;
import org.json.JSONObject;

@Path("categories")
public class ExpenseResouce {

    private static List<Category> categories = new ArrayList<>();

    static {
        Category c1 = new Category("food",
                new Expense("Burger", 15),
                new Expense("pizza", 20));

        Category c2 = new Category("toys",
                new Expense("switch", 10),
                new Expense("Bug", 25));

        categories.add(c1);
        categories.add(c2);
    }

    private JSONObject parseExp(Expense expense) {
        JSONObject json = new JSONObject();
        json.put("name", expense.getName());
        json.put("cost", expense.getCost());
        return json;
    }

    private JSONObject parseCat(Category category) {
        JSONObject json = new JSONObject();
        json.put("name", category.getName());
        JSONArray arr = new JSONArray();

        for (Expense exp : category.getExpenses()) {
            arr.put(parseExp(exp));
        }
        json.put("expenses", arr);
        return json;
    }

    @Path("")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getCategories() {
        JSONArray result = new JSONArray();

        for (Category category : categories) {
            result.put(parseCat(category));
        }
        return result.toString();
    }

    @Path("{i:[0-9]+}") //pattern aby podawane były tylko liczby
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getCategoryById(@PathParam("i") int i
    ) {
        return parseCat(categories.get(i)).toString();
    }

    private Expense parseExp(JSONObject expense) {
        Expense ex = new Expense();
        ex.setName(expense.getString("name"));
        ex.setCost(expense.getInt("cost"));
        return ex;
    }

    private Category parseCat(JSONObject category) {
        Category ca = new Category();
        ca.setName(category.getString("name"));
        JSONArray array = category.getJSONArray("expenses");
        List<Expense> ex = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            ex.add(parseExp(array.getJSONObject(i)));
        }
        ca.setExpenses(ex);
        return ca;
    }

    @Path("")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void createCategory(String json) {
        Category category = parseCat(new JSONObject(json));
        boolean found = false;
        for (Category c : categories) {
            if(category.getName().equals(c.getName())){
                c.getExpenses().addAll(category.getExpenses());
                found = true;
                break;
            }
        }
        if(!found){
            categories.add(category);
        }
    }
    
    @Path("{categoryIndex}/expenses/{expenseIndex}")
    @DELETE
    public void remove(@PathParam("categoryIndex") int categoryIndex, 
            @PathParam("expenseIndex") int expenseIndex){
        categories.get(categoryIndex).getExpenses().remove(expenseIndex);
    }

}
