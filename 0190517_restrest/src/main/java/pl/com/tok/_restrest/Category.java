package pl.com.tok._restrest;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Category {

private String name;
private List<Expense> expenses;

public Category(String name, Expense...expenses){
    this(name,new ArrayList<>());
    for (Expense expense : expenses) {
        this.expenses.add(expense);
    }
}

    public Category(String name, List<Expense> expenses) {
        this.name = name;
        this.expenses = expenses;
    }

    public Category() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(List<Expense> expenses) {
        this.expenses = expenses;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.name);
        hash = 29 * hash + Objects.hashCode(this.expenses);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Category other = (Category) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.expenses, other.expenses)) {
            return false;
        }
        return true;
    }



}
